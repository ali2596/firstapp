import React, { Component } from "react";
import { View, StyleSheet, Image, Animated } from "react-native";

export default class Splash extends Component {
    constructor(props) {
        super(props);
        this.state = {
            fadeValue: new Animated.Value(0),
        };
    };
    componentDidMount(){
        Animated.timing(this.state.fadeValue, {toValue: 1, duration: 5000,}).start()
    }
    
    render() {
        return (
            <View style={style.container} >
                <Animated.View style={{opacity: this.state.fadeValue}}>
                    <Image source={require('../assets/logo.png')} style={style.logo} />
                </Animated.View>
            </View>
        )
    }
}

const style = StyleSheet.create({
    container: {
        backgroundColor: "rgb(0,30,55)",
        alignItems: "center",
        justifyContent: "center",
        flex: 1,
    },

    logo: {
        width: 200,
        height: 200,
    }
})