import React, { Component } from "react";
import { View, Text, StyleSheet, Image, StatusBar, TextInput, SafeAreaView, TouchableOpacity, KeyboardAvoidingView, TouchableWithoutFeedback, Keyboard } from "react-native";

export default class Login extends Component {
    state = {Username: "", Password:""}

    checkLogin(){
        const {Username, Password} = this.state;
        if (Username == 'admin' && Password == 'admin'){
            console.log("Login");
        }
        else{
            console.log("Incorrect")
        }
    }
    render() {
        return (
            <SafeAreaView style={style.container}>
                <StatusBar barStyle='light-content'></StatusBar>
                <KeyboardAvoidingView behavior='padding' style={style.container}>
                    <TouchableWithoutFeedback style={style.container} onPress={Keyboard.dismiss}>
                        <View style={style.container}>
                            <View style={style.infoContainer, style.logoContainer}>
                            <Image source={require('../assets/logo.png')} style={style.logo}></Image>
                                <TextInput style={style.input}
                                    placeholder='Enter Username/Email'
                                    placeholderTextColor="white"
                                    keyboardType="email-address"
                                    returnKeyType='next'
                                    autoCorrect={false}
                                    onSubmitEditing={() => this.refs.txtPasscode.focus()}
                                    onChangeText={text => this.setState({Username: text})}
                                />
                                <TextInput style={style.input}
                                    placeholder='Enter Password'
                                    placeholderTextColor="white"
                                    returnKeyType='go'
                                    secureTextEntry
                                    autoCorrect={false}
                                    ref='txtPasscode'
                                    onChangeText={text => this.setState({Password: text})}
                                />
                                <TouchableOpacity style={style.buttonContainer} onPress={_ => this.checkLogin()}>
                                    <Text style={style.buttonText}>Sign In</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </TouchableWithoutFeedback>


                </KeyboardAvoidingView>


            </SafeAreaView>

        )
    }
}

const style = StyleSheet.create({
    container: {
        backgroundColor: "rgb(0,30,55)",
        flexDirection: "column",
        flex: 1,
        
    },
    logoContainer: {
        flex: 1,
        alignItems: "center",
        justifyContent: "center",
    },
    logo: {
        width: 200,
        height: 200,
    },
    infoContainer: {
        position: "absolute",
        left: 0,
        right: 0,
        bottom: 0,
        height: 200,
        padding: 20,
    },
    input: {
        backgroundColor: 'rgba(255,255,255,0.1)',
        height: 40,
        paddingHorizontal: 10,
        marginBottom: 20,
        color: 'rgb(255,255,255)',
        width: "80%"
    },
    buttonContainer: {
        backgroundColor: 'rgb(240,90,40)',
        paddingVertical: 15, 
        width: "80%"
    },
    buttonText: {
        textAlign: 'center',
        color: "white",
        fontSize: 18,
        fontWeight: "bold",
    }
})
